import Game from '../model/game.js'
import User from '../model/user.js'
import jwt from 'jsonwebtoken'

const JWT_SECRET = "supersecretphrasetocreatetoeknbakdlsjkfklas;"


exports.getGames = (req, res) => {
    const authHeader = req.headers["authorization"] || req.headers["Authorization"];


    if (authHeader === undefined) {
        Game.find({ isPrivate: false }).then(games => {
            res.send(games);
        })
    } else {

        const authType = authHeader.split(" ")[0];
        const authToken = authHeader.split(" ")[1];

        if (authType != "Bearer") {
            res.status(400).send("bad token syntax");
            return;
        }

        jwt.verify(authToken, JWT_SECRET, (err, decoded) => {
            if (err) {
                res.status(403).send("Unauthorized");
                console.log(err)
                return;
            }

            User.findOne({
                'username': decoded.username
            }).then(user => {
                if (user != null) {
                    Game.find({}).then(games => {
                        res.send(games);
                    })
                } else {
                    res.send(403, 'User not logged in')
                }
            })
        })
    }
}

exports.getPrivateGames = (req, res) => {
    const authHeader = req.headers["authorization"] || req.headers["Authorization"];


    if (authHeader === undefined) {
        Game.find({ isPrivate: false }).then(games => {
            res.send(games);
        })
    } else {

        const authType = authHeader.split(" ")[0];
        const authToken = authHeader.split(" ")[1];

        if (authType != "Bearer") {
            res.status(400).send("bad token syntax");
            return;
        }

        jwt.verify(authToken, JWT_SECRET, (err, decoded) => {
            if (err) {
                res.status(403).send("Unauthorized");
                console.log(err)
                return;
            }

            User.findOne({
                'username': decoded.username
            }).then(user => {
                if (user != null) {
                    Game.find({isPrivate:true}).then(games => {
                        res.send(games);
                    })
                } else {
                    res.send(403, 'User not logged in')
                }
            })
        })
    }
}

exports.getCommentsByGame = async (req, res) => {
    var game = await Game.findById(req.params.gameid)
    for (let i = 0; i < game.comments.length; i++) {
        var userComment = await User.findById(game.comments[i].author)
        game.comments[i].author = userComment
    }
    res.send(game.comments);
}

exports.getGameById = async (req, res) => {

    var game = await Game.findById(req.params.gameid)
    var user = await User.findById(game.author)
    game.author = user
    for (let i = 0; i < game.comments.length; i++) {
        var userComment = await User.findById(game.comments[i].author)
        game.comments[i].author = userComment
    }
    res.send(game)
}

exports.getGamesByCategorie = (req, res) => {

    const categorie = req.params.categorie

    const authHeader = req.headers["authorization"] || req.headers["Authorization"];


    if (authHeader === undefined) {
        Game.find({ isPrivate: false, categorie: categorie }).then(games => {
            res.send(games);
            return;
        })
    } else {

        const authType = authHeader.split(" ")[0];
        const authToken = authHeader.split(" ")[1];

        if (authType != "Bearer") {
            res.status(400).send("bad token syntax");
            return;
        }

        jwt.verify(authToken, JWT_SECRET, (err, decoded) => {
            if (err) {
                res.status(403).send("Unauthorized");
                console.log(err)
                return;
            }

            User.findOne({
                'username': decoded.username
            }).then(user => {
                if (user != null) {
                    Game.find({ categorie: categorie }).then(games => {
                        res.send(games);
                    })
                } else {
                    res.send(403, 'User not logged in')
                }
            })
        })
    }
}

exports.addGame = (req, res) => {

    const authHeader = req.headers["authorization"] || req.headers["Authorization"];

    if (!authHeader) {
        res.status(403).send("Unauthorized");
        return;
    }

    const authType = authHeader.split(" ")[0];
    const authToken = authHeader.split(" ")[1];

    if (authType != "Bearer") {
        res.status(400).send("bad token syntax");
        return;
    }

    jwt.verify(authToken, JWT_SECRET, (err, decoded) => {
        if (err) {
            res.status(403).send("Unauthorized");
            console.log(err)
            return;
        }

        User.findOne({
            'username': decoded.username
        }).then(user => {
            if (user.isAdmin === true) {
                var newGame = new Game(req.body);
                newGame.author = user._id
                newGame.date = Date.now();
                newGame.save().then(game => {
                    res.send(game);
                }).catch(err => {
                    res.send(200, err);
                })
            } else {
                res.send(403, "User not admin")
            }
        })
    })




}

exports.deleteComment = async (req, res) => {
    const authHeader = req.headers["authorization"] || req.headers["Authorization"];

    if (!authHeader) {
        res.status(403).send("Unauthorized");
        return;
    }

    const authType = authHeader.split(" ")[0];
    const authToken = authHeader.split(" ")[1];

    if (authType != "Bearer") {
        res.status(400).send("bad token syntax");
        return;
    }

    jwt.verify(authToken, JWT_SECRET, (err, decoded) => {
        if (err) {
            res.status(403).send("Unauthorized");
            console.log(err)
            return;
        }

        User.findOne({
            'username': decoded.username
        }).then(user => {
            if (user.isAdmin == true) {
                Game.findByIdAndUpdate(req.params.gameid,
                {
                    '$pull': {'comments': {'_id': req.params.commentid}}
                },
                function(err, doc) {
                    res.send(doc)
                })
            }
        })
    })
}

exports.comment = (req, res) => {

    const authHeader = req.headers["authorization"] || req.headers["Authorization"];

    if (!authHeader) {
        res.status(403).send("Unauthorized");
        return;
    }

    const authType = authHeader.split(" ")[0];
    const authToken = authHeader.split(" ")[1];

    if (authType != "Bearer") {
        res.status(400).send("bad token syntax");
        return;
    }

    jwt.verify(authToken, JWT_SECRET, (err, decoded) => {
        if (err) {
            res.status(403).send("Unauthorized");
            console.log(err)
            return;
        }

        User.findOne({
            'username': decoded.username
        }).then(user => {
            Game.findById(req.params.gameid).then(game => {
                var comment = { author: user._id, comment: req.body.comment, date: Date.now() }
                game.comments.push(comment)
                game.save()
                res.send(game)
            }).catch(err => {
                res.send(200, err)
            })
        })
    })
}