import User from '../model/user.js'
import jwt from 'jsonwebtoken'

const JWT_SECRET = "supersecretphrasetocreatetoeknbakdlsjkfklas;"

exports.login = (req, res) => {


    const auth64 = (req.headers.authorization || '').split(' ')[1] || '';
    const [login, password] = Buffer.from(auth64, 'base64').toString().split(':')

    User.findOne({
        'username': login,
        'password': password
    }).then(user => {
        if (user != null) {

            jwt.sign(user.toJSON(), JWT_SECRET, { expiresIn: 1000000000 }, (err, token) => {
                if (err) {
                    console.log(err);
                    res.status(200).send("Error: " + err);
                    return;
                }
                const response = {
                    username: login,
                    token: token
                }
                res.send(response);
            })
        } else {
            User.findOne({
                'email': login,
                'password': password
            }).then(user => {
                if (user != null) {
                    jwt.sign(user.toJSON(), JWT_SECRET, { expiresIn: 1000000000 }, (err, token) => {
                        if (err) {
                            console.log(err);
                            res.status(200).send("Error: " + err);
                            return;
                        }
                        const response = {
                            email: login,
                            token: token
                        }
                        res.send(response);
                    })
                } else {
                    res.send(400, "User not found")
                }
            })
        }
    }).catch(err => {
        res.status(200).send(err.errors);
    });
}

exports.toggleStatus = (req, res) => {
    const authHeader = req.headers["authorization"] || req.headers["Authorization"];

    if (!authHeader) {
        res.status(403).send("Unauthorized");
        return;
    }

    const authType = authHeader.split(" ")[0];
    const authToken = authHeader.split(" ")[1];

    if (authType != "Bearer") {
        res.status(400).send("bad token syntax");
        return;
    }

    jwt.verify(authToken, JWT_SECRET, (err, decoded) => {
        if (err) {
            res.status(403).send("Unauthorized");
            return;
        }

        User.findOne({
            'username': decoded.username
        }).then(user => {

            if (user == null) {
                res.send(200, "User not found")
            } else if (user.isAdmin === true) {
                User.findOne({ 'username': req.params.username }).then(userToPromote => {

                    if (userToPromote.isAdmin) {
                        userToPromote.isAdmin = false
                    } else {
                        userToPromote.isAdmin = true
                    }

                    userToPromote.save()

                    res.send(userToPromote)
                    return;

                })
            }
        })
    })
}

exports.getUsers = (req, res) => {
    User.find({}).then(users => {
        res.send(users);
    })
}

exports.addUser = (req, res) => {
    const auth64 = (req.headers.authorization || '').split(' ')[1] || '';
    const [username, password] = Buffer.from(auth64, 'base64').toString().split(':')
    var newUser = new User(req.body);
    newUser.username = username;
    newUser.password = password;
    newUser.save().then(user => {
        jwt.sign(user.toJSON(), JWT_SECRET, { expiresIn: 1000 }, (err, token) => {
            if (err) {
                console.log(err);
                res.status(200).send("Error: " + err);
                return;
            }
            const response = {
                user: user,
                token: token
            }
            res.send(response);
        })
    }).catch(err => {
        res.send(200, err)
    })
}


exports.getProfile = (req, res) => {
    const authHeader = req.headers["authorization"] || req.headers["Authorization"];

    if (!authHeader) {
        res.status(403).send("Unauthorized");
        return;
    }

    const authType = authHeader.split(" ")[0];
    const authToken = authHeader.split(" ")[1];

    if (authType != "Bearer") {
        res.status(400).send("bad token syntax");
        return;
    }

    jwt.verify(authToken, JWT_SECRET, (err, decoded) => {
        if (err) {
            res.status(200).send(err);
            return;
        }

        User.findOne({
            'username': decoded.username
        }).then(user => {
            res.send(user);
        })
    })
}