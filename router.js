import User from './controllers/userController.js'
import Game from './controllers/gameController.js'

const routes = (app) => {
    app.route('/')
        .get(getHome);

    app.route('/register')
        .post(User.addUser)

    app.route('/login')
        .post(User.login)

    app.route('/allUsers')
        .get(User.getUsers)

    app.route('/allGames')
        .get(Game.getGames)

    app.route('/privateGames')
        .get(Game.getPrivateGames)

    app.route('/addGame')
        .post(Game.addGame)

    app.route('/profile/me')
        .get(User.getProfile)

    app.route('/profile/:username')
        .put(User.toggleStatus)

    app.route('/comment/:gameid')
        .post(Game.comment)

    app.route('/jeux/:gameid/comment/:commentid')
        .delete(Game.deleteComment)

    app.route('/games/:categorie')
        .get(Game.getGamesByCategorie)

    app.route('/game/:gameid')
        .get(Game.getGameById)

    app.route('/comments/:gameid')
        .get(Game.getCommentsByGame)
}

function getHome(req, res) {
    res.send("Hello")
}

export default routes