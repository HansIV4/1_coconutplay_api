import { Schema as _Schema, model, Collection, Mongoose } from 'mongoose';
import users from "../data/users.json"


var Schema = _Schema;

var UserSchema = new Schema({
    username: {type: String, unique: true, required: true, dropDups: true},
    email: {type: String, unique: true, required: true, dropDups: true},
    password: {type: String, required: true},
    name: String ,
    firstname: String,
    birthdate: String,
    isAdmin: {type: Boolean, default:false, required: true }
}, {versionKey: false});

var User = model('User', UserSchema)

export default User

