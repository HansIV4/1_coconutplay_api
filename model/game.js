import { Schema as _Schema, model, Mongoose } from 'mongoose';


var Schema = _Schema;

var CommentSchema = new Schema({
    comment: String,
    author: {type: Schema.Types.ObjectId, ref: 'User'},
    date: Date
})

var GameSchema = new Schema({
    name: String,
    image: String,
    categorie: String,
    author : {type: Schema.Types.ObjectId, ref: 'User', required:true},
    title: String ,
    note: String,
    review: String,
    date: Date,
    comments: [CommentSchema],
    isPrivate: {type: Boolean, required:true, default:false}
}, {versionKey: false});

var Game = model('Game', GameSchema)

export default Game