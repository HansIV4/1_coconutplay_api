import express from "express"
import bodyParser from "body-parser"
import cors from 'cors'
import routes from './router.js'
import instanciate from './database.js'
import User from './model/user.js'
import users from './data/users.json'
import games from './data/games.json'
import json2mongo from 'json2mongo'
import Game from "./model/game.js"



const app = express();
const PORT = 3000;

app.use(cors())
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());



instanciate()

var users2 = json2mongo(users)
var games2 = json2mongo(games)

User.collection.insert(users2, onInsert)
Game.collection.insert(games2, onInsert)


function onInsert(err, docs) {
    
}

routes(app);

app.use((req, res) => {
    res.status(404).send({url: req.originalUrl + 'not found'})
});

app.listen(PORT, () => {
    console.log(`server running on port ${PORT}`)
});